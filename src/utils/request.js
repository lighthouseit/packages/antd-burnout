/**
 * request library
 * https://github.com/umijs/umi-request
 */
import { extend } from 'umi-request';
import { notification } from 'antd';

const codeMessage = {
  200: 'O servidor retornou com sucesso os dados solicitados.',
  201: 'Dados novos ou modificados são bem-sucedidos.',
  202: 'Uma solicitação entrou na fila de segundo plano (tarefa assíncrona).',
  204: 'Os dados foram excluídos com sucesso.',
  // eslint-disable-next-line max-len
  400: 'A solicitação foi feita com um erro e o servidor não executou nenhuma operação para criar ou modificar dados.',
  401: 'O usuário não tem permissão (token, nome de usuário ou senha está incorreto).',
  403: 'O usuário está autorizado, mas o acesso é proibido.',
  404: 'A solicitação é feita para um registro que não existe.',
  406: 'O formato da solicitação não está disponível.',
  410: 'O recurso solicitado é permanentemente excluído e não será recuperado.',
  422: 'Ocorreu um erro de validação ao criar um objeto.',
  500: 'Ocorreu um erro no servidor Por favor, verifique o servidor.',
  502: 'Erro de gateway.',
  503: 'O serviço está indisponível e o servidor está temporariamente sobrecarregado.',
  504: 'O gateway expirou.',
};

/**
 * Manipulador de exceções
 */
const errorHandler = (error) => {
  const { response = {} } = error;
  const errortext = codeMessage[response.status] || response.statusText;
  const { status, url } = response;
  notification.error({
    message: `Erro na solicitação ${status}: ${url}`,
    description: errortext,
  });
};

/**
 * Configuração padrão da request
 */
const request = extend({
  errorHandler, // Tratamento de erros
  credentials: 'include', // Se a solicitação padrão vem com um cookie
});
export default request;
